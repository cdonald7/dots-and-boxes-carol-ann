package dotsandboxes;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DotsAndBoxesGridTest {
    private static final Logger logger = LogManager.getLogger(DotsAndBoxesGridTest.class);

    @Test
    public void testTestSuiteRuns() {
        logger.info("Dummy test to show the test suite runs");
        assertTrue(true);
    }

    @Test
    public void testSquareCompletion() {
        logger.info("Testing if square completion detection is working");

        // Initialize the grid with dimensions and number of players
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);
        
        // Simulate drawing lines to complete a square at (0,0)
        grid.drawHorizontal(0, 0, 1);
        grid.drawVertical(0, 0, 1);
        grid.drawHorizontal(0, 1, 1);
        grid.drawVertical(1, 0, 1);

        // Check if the square at (0,0) is complete
        assertTrue(grid.boxComplete(0, 0), "Square at (0,0) should be complete");
    }

    @Test
    public void testDrawingLineTwiceThrowsException() {
        logger.info("Testing if drawing a line twice throws an IllegalStateException");

        // Initialize the grid with dimensions and number of players
        DotsAndBoxesGrid grid = new DotsAndBoxesGrid(3, 3, 2);

        // Simulate drawing a horizontal line
        grid.drawHorizontal(0, 0, 1);

        // Try drawing the same horizontal line again, which should throw an IllegalStateException
        assertThrows(IllegalStateException.class, () -> grid.drawHorizontal(0, 0, 1));
    }
}

